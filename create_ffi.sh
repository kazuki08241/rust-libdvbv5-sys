#!/bin/sh

function_regex="(atsc|dvb|isdb|isdbt)_.*"
type_regex="(atsc|dvb|dvbsat|dmx|dtv|fe|isdb|isdbt|ts)_.*"
var_regex="(ATSC|channel|DMX|DTV|dvb|DVB|dvbc|dvbs|dvbt|dvbt2|FE_TUNE|fe|isdbt|LIBDVBV5|LNA|LOG|MAX_DELIVERY|MAX_DTV|MAX_TABLE|NO_STREAM|pmt|sys)_.*"

bindgen \
    --whitelist-function "$function_regex" \
    --whitelist-type "$type_regex" \
    --whitelist-var "$var_regex" \
    --default-enum-style rust \
    wrapper.h > ffi.rs
