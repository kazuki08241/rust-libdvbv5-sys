# Rust-libdvbv5-sys

GitlLab CI: [![GitLab CI](https://gitlab.com/Russel/rust-libdvbv5-sys/badges/master/pipeline.svg)](https://gitlab.com/Russel/rust_libdvbv5_sys)
&nbsp;&nbsp;&nbsp;&nbsp;
Licence: [![Licence](https://img.shields.io/badge/license-LGPL_3-green.svg)](https://www.gnu.org/licenses/lgpl-3.0.en.html)

Rust FFI for the
[libdvbv5 library](https://linuxtv.org/docs/libdvbv5/)
from the
[Video for Linux](https://www.linuxtv.org/wiki/index.php/Development:_Video4Linux_APIs)
project that is part of the
[Linux TV](https://www.linuxtv.org/) effort.

The binding in version 0.1.x of this crate was generated from libdvbv5 1.20.0 on
Debian Sid. GitLab CI tests are run against libdvbv5 1.16.3 on Debian Buster, the platform
used for the Rust latest Docker image. 

## Licence

This code is licenced under LGPLv3.
[![Licence](https://www.gnu.org/graphics/lgplv3-147x51.png)](https://www.gnu.org/licenses/lgpl-3.0.en.html)
