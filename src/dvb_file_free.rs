/*
 *  libdvbv5-sys — a Rust FFI binding for the libdvbv5 library from V4L2.
 *
 *  Copyright © 2019, 2020  Russel Winder
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

use libc;

use crate::ffi;

/// Free a `dvb_file` instance.
///
/// It is assumed that all "objects" are on the heap. If any are on the stack then
/// a SIGABRT will result. This seems entirely reasonable.
///
/// `dvb_file_free` is a function-like macro in the C code which Bindgen does not handle.
/// This is a manually written function based on the function-like macro definition.
pub fn dvb_file_free(file: *mut ffi::dvb_file) {
    if !file.is_null() {
        unsafe {
            let mut entry: *mut ffi::dvb_entry = (*file).first_entry;
            while !entry.is_null() {
                if !(*entry).channel.is_null() {
                    libc::free((*entry).channel as *mut libc::c_void);
                }
                if !(*entry).vchannel.is_null() {
                    libc::free((*entry).vchannel as *mut libc::c_void);
                }
                if !(*entry).location.is_null() {
                    libc::free((*entry).location as *mut libc::c_void);
                }
                if !(*entry).video_pid.is_null() {
                    libc::free((*entry).video_pid as *mut libc::c_void);
                }
                if !(*entry).audio_pid.is_null() {
                    libc::free((*entry).audio_pid as *mut libc::c_void);
                }
                if !(*entry).other_el_pid.is_null() {
                    libc::free((*entry).other_el_pid as *mut libc::c_void);
                }
                if !(*entry).lnb.is_null() {
                    libc::free((*entry).lnb as *mut libc::c_void);
                }
                let next = (*entry).next;
                libc::free(entry as *mut libc::c_void);
                entry = next;
            }
            libc::free(file as *mut libc::c_void);
        }
    }
}
