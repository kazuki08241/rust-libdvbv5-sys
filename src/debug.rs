/*
 *  libdvbv5-sys — a Rust FFI binding for the libdvbv5 library from V4L2.
 *
 *  Copyright © 2019, 2020  Russel Winder
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

//! Various implementations of std::fmt::Debug for types that bindgen does not
//! annotate with #[derive(Debug)]

use crate::ffi;

/// A manually written implementation of `Debug` trait for `dvb_frontend_info`.
///
/// Bindgen does not mark `dvb_frontend_info` as `#[derive(Debug)]` because of the array
/// entry `name` being >32 items in length and so not having a `Debug` definition.
/// Hence this manual realisation.
impl std::fmt::Debug for ffi::dvb_frontend_info {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "dvb_frontend_info{{\
            name: {:?}…,\
            type_: {:?},\
            frequency_min: {:?},\
            frequency_max: {:?},\
            frequency_stepsize: {:?},\
            frequency_tolerance: {:?},\
            symbol_rate_min: {:?},\
            symbol_rate_max: {:?},\
            symbol_rate_tolerance: {:?},\
            notifier_delay: {:?},\
            caps: {:?}}}",
            &self.name[..32], // NB Only first 32 of 128 chars.
            &self.type_,
            &self.frequency_min,
            &self.frequency_max,
            &self.frequency_stepsize,
            &self.frequency_tolerance,
            &self.symbol_rate_min,
            &self.symbol_rate_max,
            &self.symbol_rate_tolerance,
            &self.notifier_delay,
            &self.caps,
        )
    }
}

/// A manually written implementation of `Debug` for `dvb_v5_fe_parms`.
///
/// Bindgen does not mark `dvb_v5_fe_parms` as `#[derive(Debug)#` because the `info` field
/// is a [dvb_frontend_info](struct.dvb_frontend_info.html) which is not marked
/// `#[derive(Debug)]`.  Hence this manual realisation.
impl std::fmt::Debug for ffi::dvb_v5_fe_parms {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "dvb_v5_fe_parms{{\
            info: {:?},\
            version: {:?},\
            has_v5_stats: {:?},\
            current_sys: {:?},\
            num_systems: {:?},\
            systems: {:?},\
            legacy_fe: {:?},\
            abort: {:?},\
            lna: {:?},\
            lnb: {:?},\
            sat_number: {:?},\
            freq_bpf: {:?},\
            diseqc_wait: {:?},\
            verbose: {:?},\
            logfunc: {:?},\
            default_charset: {:?},\
            output_charset: {:?}}}",
            &self.info,
            &self.version,
            &self.has_v5_stats,
            &self.current_sys,
            &self.num_systems,
            &self.systems,
            &self.legacy_fe,
            &self.abort,
            &self.lna,
            &self.lnb,
            &self.sat_number,
            &self.freq_bpf,
            &self.diseqc_wait,
            &self.verbose,
            &self.logfunc,
            &self.default_charset,
            &self.output_charset,
        )
    }
}

/// A manually written implementation of `Debug` for `dtv_property`.
///
/// Bindgen does not mark `dtv_property` as `#[derive(Debug)#` because the `u` field
/// is a bindgen generated union.  Hence this manual realisation.
impl std::fmt::Debug for ffi::dtv_property {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        unsafe {
            write!(
                f,
                "dtv_property{{\
            cmd: {:?},\
            reserved: {:?},\
            u: …,\
            result: {:?}}}",
                &self.cmd,
                &self.reserved,
                // &self.u, // TODO See how to output the union.
                &self.result,
            )
        }
    }
}

/// A manually written implementation of `Debug` for `dvb_entry`.
///
/// Bindgen does not mark `dvb_entry` as `#[derive(Debug)#` because the `props` field
/// is an array of 70 `dtv_property` items.  Hence this manual realisation.
impl std::fmt::Debug for ffi::dvb_entry {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "dvb_entry{{\
            props: {:?}…,\
            n_props: {:?},\
            next: {:?},\
            service_id: {:?},\
            video_pid: {:?},\
            audio_pid: {:?},\
            other_el_pid: {:?},\
            video_pid_len: {:?},\
            audio_pid_len: {:?},\
            other_el_pid_len: {:?},\
            channel: {:?},\
            vchannel: {:?},\
            location: {:?},\
            sat_number: {:?},\
            freq_bpf: {:?},\
            diseqc_wait: {:?},\
            lnb: {:?},\
            network_id: {:?},\
            transport_id: {:?}}}",
            &self.props[0..32],
            &self.n_props,
            &self.next,
            &self.service_id,
            &self.video_pid,
            &self.audio_pid,
            &self.other_el_pid,
            &self.video_pid_len,
            &self.audio_pid_len,
            &self.other_el_pid_len,
            &self.channel,
            &self.vchannel,
            &self.location,
            &self.sat_number,
            &self.freq_bpf,
            &self.diseqc_wait,
            &self.lnb,
            &self.network_id,
            &self.transport_id,
        )
    }
}
